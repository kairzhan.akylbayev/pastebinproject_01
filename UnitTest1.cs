#pragma warning disable NUnit1032 // An IDisposable field/property should be Disposed in a TearDown method
using NUnit.Framework;
using NUnit.Framework.Legacy;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;

namespace PasteBinTest
{
    public class Tests
    {
        private IWebDriver driver;
        private WebDriverWait wait;

        [SetUp]
        public void Setup()
        {
            // Set up WebDriver
            driver = new ChromeDriver();
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void TearDown()
        {
            // Clean up WebDriver
            driver.Quit();
        }

        [Test]
        public void TestCreateNewPaste()
        {
            // Open the website
            driver.Navigate().GoToUrl("https://privatebin.net/");

            // Find and click on the New Paste button
            IWebElement newPasteButton = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.Id("newbutton")));
            newPasteButton.Click();

            // Fill in the paste details
            PastePage pastePage = new PastePage(driver);
            pastePage.FillPasteDetails("git config --global user.name \"New Sheriff in Town\"", "�������� ���", "10 Minutes");

            // Save the paste
            pastePage.SavePaste();

            ClassicAssert.IsTrue(pastePage.IsCodeCorrect("git config --global user.name \"New Sheriff in Town\""), "Code mismatch");
        }
    }
}
