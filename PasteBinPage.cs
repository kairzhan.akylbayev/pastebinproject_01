﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;

namespace PasteBinTest
{
    public class PastePage
    {
        private readonly IWebDriver driver;

        public PastePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void FillPasteDetails(string code, string syntax, string expiration)
        {
            // Fill in paste details
            IWebElement codeTextarea = driver.FindElement(By.Id("message"));
            codeTextarea.SendKeys(code);

            // Make syntax dropdown visible
            IJavaScriptExecutor jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].style.display='block';", driver.FindElement(By.Id("pasteFormatter")));

            // Click on the syntax dropdown to open it
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

            MakeElementVisible("pasteFormatter");

            // Make syntax option dropdown visible
            IWebElement syntaxOptionDropdown = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("pasteFormatter")));
            IJavaScriptExecutor jsExecutorE = (IJavaScriptExecutor)driver;
            jsExecutorE.ExecuteScript("arguments[0].style.display='block';", syntaxOptionDropdown);

            // Select syntax option
            SelectElement syntaxSelect = new SelectElement(syntaxOptionDropdown);
            syntaxSelect.SelectByValue("syntaxhighlighting");

            MakeElementVisible("pasteExpiration");

            // Make expiration dropdown visible
            IWebElement expirationDropdown = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("pasteExpiration")));
            jsExecutor.ExecuteScript("arguments[0].style.display='block';", expirationDropdown);

            // Select expiration option
            SelectElement expirationSelect = new SelectElement(expirationDropdown);
            expirationSelect.SelectByValue("10min");
        }

        private void MakeElementVisible(string elementId)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("document.getElementById('" + elementId + "').classList.remove('hidden');");
        }
        public void SavePaste()
        {
            // Save the paste
            IWebElement saveButton = driver.FindElement(By.Id("sendbutton"));
            saveButton.Click();
        }

        public bool IsCodeCorrect(string expectedCode)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#prettymessage pre")));

            // Get the text of the code element
            IWebElement codeElement = driver.FindElement(By.CssSelector("#prettymessage pre"));
            string codeText = codeElement.Text;

            // Check if the expected code is present in the text
            return codeText.Contains(expectedCode);
        }
    }
}
